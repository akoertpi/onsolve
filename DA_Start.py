import json
import sys
import requests
from veracode_api_signing.plugin_requests import RequestsAuthPluginVeracodeHMAC
api_url = "https://api.veracode.com/was/configservice/v1/analyses"
json_data = None


vid = sys.argv[1] 
vkey = sys.argv[2] 
guid = sys.argv[3]
DAConfig = sys.argv[4]

#headers = {"User-Agent": "Dynamic Analysis API"}
headers = {'User-Agent': 'HTTPie/2.0.0', 'Content-Type': 'application/json', 'Accept': 'application/json'}

def patch_schedule(id, scan_config):
    print("Patching existing job id:" + id)
    print("Using the following config")
    with open(scan_config, encoding=('utf-8-sig')) as json_file: 
      json_data= json.load(json_file)
    print(json_data)
    #api_uri = "https://api.veracode.com/was/configservice/v1/analyses/942fa6229c0d52f87317ddaa2a83f7d4?method=PATCH"
    api_uri = "https://api.veracode.com/was/configservice/v1/analyses/"  + id + "?method=PATCH"
    print(api_uri)
    response = requests.put(
        api_uri, auth=RequestsAuthPluginVeracodeHMAC(vid, vkey), headers=headers, json=(json_data))
    if ((response.status_code.__str__())=="204"):
        print("Dynamic Analysis Started")
        print(response.status_code.__str__())
    else:
        print("Error Running Dynamic Analysis")
        print("API reported error code: " +
              response.status_code.__str__())
        print(response.text)
        print(response.json())
      

patch_schedule(guid,DAConfig)
