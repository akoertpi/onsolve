import json
import sys
import requests
from veracode_api_signing.plugin_requests import RequestsAuthPluginVeracodeHMAC
api_url = "https://api.veracode.com/was/configservice/v1/analyses"
json_data = None


vid = sys.argv[1] 
vkey = sys.argv[2] 
DAconfig = sys.argv[3]

#headers = {"User-Agent": "Dynamic Analysis API"}
headers = {'User-Agent': 'HTTPie/2.0.0', 'Content-Type': 'application/json', 'Accept': 'application/json'}

def Create_DA(config):
    print("Creating Dynamic Analyis with the following config")
    print(config)
    with open(config, encoding=('utf-8-sig')) as json_file: 
      json_data= json.load(json_file)
    print(json_data)
    
    api_uri = "https://api.veracode.com/was/configservice/v1/analyses"
    print(api_uri)
    response = requests.post(
        api_uri, auth=RequestsAuthPluginVeracodeHMAC(vid, vkey), headers=headers, json=(json_data))
    if ((response.status_code.__str__())=="201"):
        print("Dynamic Analysis Created and Scheduled")
        print(response.status_code.__str__())
    else:
        print("Error Running Dynamic Analysis")
        print("API reported error code: " +
              response.status_code.__str__())
        print(response.text)
        print(response.json())
      

Create_DA(DAconfig)

